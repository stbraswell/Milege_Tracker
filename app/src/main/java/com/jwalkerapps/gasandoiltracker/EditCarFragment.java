package com.jwalkerapps.gasandoiltracker;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v13.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import static android.app.Activity.RESULT_OK;
import static android.text.TextUtils.isEmpty;

/**
 * Created by Troy on 3/22/2018.
 */

public class EditCarFragment extends Fragment implements View.OnClickListener {
    private DBHelper mydb ;
    SummaryFragment_old addCar ;
    //private static String[] carInfo = new String[5];
    private static String carInfo;
    public static String carInfo_oil_type;
    public static final String IMAGE_TYPE = "image/*";
    // this is the action code we use in our intent,
    // this way we know we're looking at the response from our own action
    private static final int SELECT_SINGLE_PICTURE = 101;
    private ImageView selectedImagePreview;
    static Uri selectedImageUri = null;
    String car_table = "cars";
    ArrayList carListGas;
    int pic_flag = 0;
    private static final int PERMISSION_REQUEST_WRITE= 1;
    private View mLayout;
    Uri userPic;
    String position;
    Bitmap bitmap;
    String carYear,carMake,carModel,carOilType,carOilQuant,carOilInterval,carPicture;
    public EditCarFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_add_car_fragement, container, false);
        getActivity().setTitle("Add New Vehicle");
        mydb = new DBHelper(getActivity());
        // Get Bundled data
        Bundle b = getArguments();
        //String s = b.getString("car");
        position = b.getString("position");

        // Set click listener on save button
        Button btn1 = v.findViewById(R.id.btn_add_car);
        btn1.setOnClickListener(this);

        //ImageView photoBtn = v.findViewById(R.id.add_car_photo);
        // Set click listener on picture
        selectedImagePreview = v.findViewById(R.id.add_car_photo);
        selectedImagePreview.setOnClickListener(this);

        // Get Car Data
        /*carListGas = mydb.getAllContacts(car_table);
        String carName = carListGas.get(position+1).toString();*/
        ArrayList carInfo = mydb.getIndivCarInfo(position);
        Log.d("CARINFO", String.valueOf(carInfo));

        carYear = carInfo.get(0).toString();
        carMake = carInfo.get(1).toString();
        carModel = carInfo.get(2).toString();
        carOilType = carInfo.get(3).toString();
        carOilQuant = carInfo.get(4).toString();
        carOilInterval = carInfo.get(5).toString();
        carPicture = carInfo.get(6).toString();

        // Table of Car Info
        /*public static final String CARINFO_TABLE_NAME = "cars";
        public static final String CARINFO_COLUMN_ID = "id";
        public static final String CARINFO_COLUMN_YEAR = "YEAR";
        public static final String CARINFO_COLUMN_MAKE = "MAKE";
        public static final String CARINFO_COLUMN_MODEL = "MODEL";
        public static final String CARINFO_COLUMN_OIL_TYPE = "OIL_TYPE";
        public static final String CARINFO_COLUMN_OIL_QUANT = "OIL_QUANTITY";
        public static final String CARINFO_COLUMN_OIL_INTERVAL = "OIL_INTERVAL";
        public static final String CARINFO_COLUMN_URI = "PICTURE";*/

        // Update Picture
        userPic = Uri.parse(String.valueOf(carPicture));
        try {
            selectedImagePreview.setImageBitmap(new UserPicture(userPic, getContext().getContentResolver()).getBitmap());
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set Year
        TextView year = v.findViewById(R.id.add_year);
        year.setText(carYear);

        // Set Make
        TextView make = v.findViewById(R.id.add_make);
        make.setText(carMake);

        // Set Model
        TextView model = v.findViewById(R.id.add_Model);
        model.setText(carModel);

        // Set Oil Spinner
        mLayout = getActivity().findViewById(R.id.main_layout);
        List<String> oilTypeList = Arrays.asList("0w30", "0w40", "5w20", "5w30", "5w40","10w40","10w60","15w40");
        final Spinner oilTypeDropdown = v.findViewById(R.id.add_oil);
        oilTypeDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                carInfo_oil_type = oilTypeDropdown.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> oilType_adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, oilTypeList);
        oilType_adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        oilTypeDropdown.setAdapter(oilType_adapter);
        oilTypeDropdown.setSelection(0);

        // Set Oil Quantity
        TextView oil_quant = v.findViewById(R.id.add_oil_quant);
        oil_quant.setText(carOilQuant);

        // Set Oil Interval
        TextView oil_milege = v.findViewById(R.id.add_oil_interval);
        oil_milege.setText(carOilInterval);

        return v;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add_car:
                mydb = new DBHelper(getActivity());
                addCar= new SummaryFragment_old();

                // Get text from each field
                TextView year = getView().findViewById(R.id.add_year);
                String carInfo_year = year.getText().toString();
                TextView make = getView().findViewById(R.id.add_make);
                String carInfo_make = make.getText().toString();
                TextView model = getView().findViewById(R.id.add_Model);
                carInfo = model.getText().toString();
                TextView oil_quant = getView().findViewById(R.id.add_oil_quant);
                String carInfo_oil_quant = oil_quant.getText().toString();
                TextView oil_milege = getView().findViewById(R.id.add_oil_interval);
                String carInfo_oil_milege = oil_milege.getText().toString();

                // Check if fields are filled out
                if (isEmpty(carInfo_make)){
                    Toast.makeText(getActivity(), "Please specify the MAKE of the car" , Toast.LENGTH_LONG).show();
                    return;
                } else if (isEmpty(carInfo)){
                    Toast.makeText(getActivity(), "Please specify the MODEL of the car" , Toast.LENGTH_LONG).show();
                    return;
                } else if (isEmpty(carInfo_year)) {
                    Toast.makeText(getActivity(), "Please specify the YEAR of the car", Toast.LENGTH_LONG).show();
                    return;
                } else if (isEmpty(carInfo_oil_quant)){
                    Toast.makeText(getActivity(), "Please specify the QUANTITY of oil needed" , Toast.LENGTH_LONG).show();
                    return;
                } else if (isEmpty(carInfo_oil_milege)) {
                    Toast.makeText(getActivity(), "Please specify the OIL CHANGE INTERVAL" , Toast.LENGTH_LONG).show();
                    return;
                }


                //Save picture, if any
                if (selectedImageUri != null && !selectedImageUri.equals(Uri.EMPTY)) {
                    try {
                        bitmap = new UserPicture(selectedImageUri, getContext().getContentResolver()).getBitmap();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    selectedImageUri = scaleImage(bitmap);
                    //carListGas = mydb.getAllContacts(car_table);

                    String filename = "car_" + position + ".jpg";

                    // Delete old pic
                    String dir = getContext().getFilesDir().getAbsolutePath();
                    File fileToDelete = new File(getContext().getFilesDir(),filename);
                    boolean d0 = fileToDelete.delete();
                    Log.w("Delete Check", "File deleted: " + dir + "/" + filename + " " + d0);


                    File data = Environment.getDataDirectory();
                    String appPath = "//data//" + "com.jwalkerapps.gasandoiltracker"
                            + "//files//";
                    File myPath = new File(data, appPath);
                    InputStream in = null;

                    try {
                        in = getContext().getContentResolver().openInputStream(selectedImageUri);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    OutputStream out = null;
                    try {
                        out = new FileOutputStream(new File(getContext().getFilesDir(),filename));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    byte[] buf = new byte[1024];
                    int len;
                    try {
                        while((len=in.read(buf))>0){
                            out.write(buf,0,len);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    File testFile = new File(getContext().getFilesDir(),filename);
                    Uri myCarPic  = Uri.fromFile(testFile);
                    Log.d("NEW URI:", String.valueOf(myCarPic));
                    // Create table for car
                    // mydb.createNewCar(carInfo_year, carInfo_make, carInfo, carInfo_oil_type, carInfo_oil_quant, getActivity());
                    //Insert info into table
                    mydb.updateCarInfo(carInfo_year, carInfo_make, carInfo, carInfo_oil_type, carInfo_oil_quant, carInfo_oil_milege, String.valueOf(myCarPic), position);
                } else {
                    selectedImageUri = userPic; //Uri.parse("android.resource://com.jwalkerapps.gasandoiltracker/" + R.drawable.car1a);
                    mydb.updateCarInfo(carInfo_year, carInfo_make, carInfo, carInfo_oil_type, carInfo_oil_quant, carInfo_oil_milege, carPicture, position);
                }


                // Clear text in each field
                make.setText("");
                model.setText("");
                year.setText("");
                oil_quant.setText("");
                oil_milege.setText("");
                // Make selectedImageUri null - otherwise if no pic is added to next car, the old pic will be used.
                selectedImageUri = null;
                // Change Fragment back to summary page
                getActivity().setTitle("Summary");
                SummaryFragment_old fragment = new SummaryFragment_old();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_place, fragment, "fragment3");
                fragmentTransaction.commit();
                break;
            case R.id.add_car_photo:
                getUSerPic();
                break;
        }



    }
    private void getUSerPic() {
        // BEGIN_INCLUDE(startCamera)
        // Check if the Camera permission has been granted
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            // Permission is already available, start camera preview
            Snackbar.make(mLayout,
                    R.string.camera_permission_available,
                    Snackbar.LENGTH_SHORT).show();

            // Request external_read permission here
            Log.d("here1","Im in get user pic");
            //Toast.makeText(getContext(), "You Clicked the Pic!", Toast.LENGTH_SHORT).show();
            // in onCreate or any event where your want the user to
            // select a file
            Intent intent = new Intent();
            intent.setType(IMAGE_TYPE);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent,
                    getString(R.string.select_picture)), SELECT_SINGLE_PICTURE);

        } else {
            // Permission is missing and must be requested.
            Log.d("PERMISSIONS","Need to Request Permissions");
            requestCameraPermission();
        }

    }

    /**
     * Requests the {@link android.Manifest.permission#CAMERA} permission.
     * If an additional rationale should be displayed, the user has to launch the request from
     * a SnackBar that includes additional information.
     */
    private void requestCameraPermission() {
        // Permission has not been granted and must be requested.
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // Display a SnackBar with cda button to request the missing permission.
            Log.d("PERMISSIONS","Requesting Permissions with explanation");
            Snackbar.make(mLayout, R.string.camera_access_required,
                    Snackbar.LENGTH_INDEFINITE).setAction(R.string.ok, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Request the permission
                    requestPermissions(
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PERMISSION_REQUEST_WRITE);
                }
            }).show();

        } else {
            Log.d("PERMISSIONS","Requesting Permissions without explanation");
            Snackbar.make(mLayout, R.string.camera_unavailable, Snackbar.LENGTH_SHORT).show();
            // Request the permission. The result will be received in onRequestPermissionResult().
            requestPermissions(
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_WRITE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.d("PERMISSIONS","Inside Permission Result");
        Log.d("PERMISSIONS","requestCode: " + requestCode);
        Log.d("PERMISSIONS","PERMISSION_REQUEST_WRITE: " + PERMISSION_REQUEST_WRITE);
        // BEGIN_INCLUDE(onRequestPermissionsResult)
        if (requestCode == PERMISSION_REQUEST_WRITE) {
            // Request for camera permission.
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission has been granted. Start camera preview Activity.
                Snackbar.make(mLayout, R.string.camera_permission_granted,
                        Snackbar.LENGTH_SHORT)
                        .show();
                // Request external_read permission here
                Log.d("here2","Im in onRequestPermissionsResult");
                //Toast.makeText(getContext(), "You Clicked the Pic!", Toast.LENGTH_SHORT).show();
                // in onCreate or any event where your want the user to
                // select a file
                Intent intent = new Intent();
                intent.setType(IMAGE_TYPE);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,
                        getString(R.string.select_picture)), SELECT_SINGLE_PICTURE);
            } else {
                // Permission request was denied.
                Snackbar.make(mLayout, R.string.camera_permission_denied,
                        Snackbar.LENGTH_SHORT)
                        .show();
            }
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_SINGLE_PICTURE) {

                selectedImageUri = data.getData();
                Log.d("ADD Car", String.valueOf(selectedImageUri));
                try {
                    selectedImagePreview.setImageBitmap(new UserPicture(selectedImageUri, getContext().getContentResolver()).getBitmap());
                    pic_flag = 1;
                } catch (IOException e) {
                    Log.e(MainActivity.class.getSimpleName(), "Failed to load image", e);
                }
            }
        }else {
            // report failure
            Toast.makeText(getContext(), R.string.msg_failed_to_get_intent_data, Toast.LENGTH_LONG).show();
            Log.d(MainActivity.class.getSimpleName(), "Failed to get intent data, result code is " + resultCode);
        }

    }
    private Uri scaleImage(Bitmap bitmap) throws NoSuchElementException {
        // Get current dimensions AND the desired bounding box
        int width = 0;

        try {
            width = bitmap.getWidth();
        } catch (NullPointerException e) {
            throw new NoSuchElementException("Can't find bitmap on given view/drawable");
        }

        int height = bitmap.getHeight();
        int bounding = dpToPx(200);
        Log.i("Test", "original width = " + Integer.toString(width));
        Log.i("Test", "original height = " + Integer.toString(height));
        Log.i("Test", "bounding = " + Integer.toString(bounding));

        // Determine how much to scale: the dimension requiring less scaling is
        // closer to the its side. This way the image always stays inside your
        // bounding box AND either x/y axis touches it.
        float xScale = ((float) bounding) / width;
        float yScale = ((float) bounding) / height;
        float scale = (xScale <= yScale) ? xScale : yScale;
        Log.i("Test", "xScale = " + Float.toString(xScale));
        Log.i("Test", "yScale = " + Float.toString(yScale));
        Log.i("Test", "scale = " + Float.toString(scale));

        // Create a matrix for the scaling and add the scaling data
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);

        // Create a new bitmap and convert it to a format understood by the ImageView
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        width = scaledBitmap.getWidth(); // re-use
        height = scaledBitmap.getHeight(); // re-use
        BitmapDrawable result = new BitmapDrawable(scaledBitmap);
        Log.i("Test", "scaled width = " + Integer.toString(width));
        Log.i("Test", "scaled height = " + Integer.toString(height));

        Uri newURI = getImageUri(scaledBitmap);
        return newURI;
    }

    private int dpToPx(int dp) {
        float density = getActivity().getResources().getDisplayMetrics().density;
        return Math.round((float)dp * density);
    }
    private Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getContext().getContentResolver(), inImage, "Title", null);
        Log.d("BITMAP2URI_editCar",path);
        return Uri.parse(path);
    }
}

package com.jwalkerapps.gasandoiltracker;

class Person {
    String year;
    String name;
    String age;
    String oil;
    String photoId;
    String id;

    Person(String year, String name, String age, String oil, String photoId,String id) {
        this.year = year;
        this.name = name;
        this.age = age;
        this.oil = oil;
        this.photoId = photoId;
        this.id = id;
    }
}
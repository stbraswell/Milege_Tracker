package com.jwalkerapps.gasandoiltracker;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class GasFragment extends Fragment implements View.OnClickListener {
String text;
    private DBHelper mydb ;

    public GasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_gas, container, false);
        getActivity().setTitle("Add Fill Up");
        Button btn1 = (Button) v.findViewById(R.id.btn1);
        final Spinner gas_dropdown_cars = (Spinner) v.findViewById(R.id.spinner);
        //Button btn2 = (Button)v.findViewById(R.id.btn2);

        btn1.setOnClickListener(this);
        gas_dropdown_cars.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                text = gas_dropdown_cars.getSelectedItem().toString();
                // Get ID
                String Carid = mydb.getID(position);
                text = text + Carid + "_GAS";
                Log.d("GAS FRAG",text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //btn2.setOnClickListener(this);

        // Populate dropdown list with all cars
        mydb = new DBHelper(getActivity());
        ArrayList<String> car_list = new ArrayList<>();
        car_list = mydb.getAllContacts("cars");
        ArrayAdapter<String> carList_adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, car_list);
        carList_adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        gas_dropdown_cars.setAdapter(carList_adapter);
        gas_dropdown_cars.setSelection(0);


        DatePicker simpleDatePicker = (DatePicker) v.findViewById(R.id.gas_datepicker); // initiate a date picker

        //simpleDatePicker.setSpinnersShown(false); // set false value for the spinner shown function
        simpleDatePicker.setCalendarViewShown(false);

// DELETE - this snipit was a test to calc avg mpg  and display on gas fragment page
        /*//Get number of rows in db
        int numRows = mydb.numberOfRows();
        //String numRows_str = Integer.toString(numRows);
        TextView rows = (TextView) v.findViewById(R.id.numrowlist);
        rows.setText("# of rows: " +Integer.toString(numRows));*/



// DELETE - this snipit was a test to grab all of the data for a given ID
        /*int id=1;
        Cursor rowdata = mydb.getData(id);

        rowdata.moveToFirst();

        String date_col = rowdata.getString(rowdata.getColumnIndex(DBHelper.CONTACTS_COLUMN_NAME));
        String time_col = rowdata.getString(rowdata.getColumnIndex(DBHelper.CONTACTS_COLUMN_TIME));
        String notes_col = rowdata.getString(rowdata.getColumnIndex(DBHelper.CONTACTS_COLUMN_PHONE));
        String miles_col = rowdata.getString(rowdata.getColumnIndex(DBHelper.CONTACTS_COLUMN_EMAIL));
        String price_col = rowdata.getString(rowdata.getColumnIndex(DBHelper.CONTACTS_COLUMN_STREET));
        String gallons_col = rowdata.getString(rowdata.getColumnIndex(DBHelper.CONTACTS_COLUMN_CITY));
        String avg_col = rowdata.getString(rowdata.getColumnIndex(DBHelper.CONTACTS_COLUMN_AVG));

        if (!rowdata.isClosed())  {
            rowdata.close();
        }
        int gasAVG = mydb.updateGasAvg();
        TextView testrow = (TextView) v.findViewById(R.id.testline);
        testrow.setText("WRX Avg: "+ gasAVG);*/
        return v;
    }


/*    public void createdb(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        db.execSQL(
                "create table gasgas " +
                        "(id integer primary key, date text,newMiles_str text,newPrice_str text, newGallons_str text,notes text)"
        );
    }*/



    @Override
    public void onClick(View v) {


//Get values from Fill up
        TextView newMiles_str = (TextView) getView().findViewById(R.id.gas_miles);
        double newMiles_int = Double.parseDouble( newMiles_str.getText().toString() );
        newMiles_str.setText("");

        TextView newPrice_str = (TextView) getView().findViewById(R.id.gas_price);
        double newPrice_int = Double.parseDouble( newPrice_str.getText().toString() );
        newPrice_str.setText("");
        TextView newGallons_str = (TextView) getView().findViewById(R.id.gas_gallons);
        double newGallons_int = Double.parseDouble( newGallons_str.getText().toString() );
        newGallons_str.setText("");
        double mpg= newMiles_int/newGallons_int;
        double roundOff = Math.round(mpg * 100.0) / 100.0;

        TextView notes = (TextView) getView().findViewById(R.id.gas_notes);


        DatePicker simpleDatePicker = (DatePicker) getView().findViewById(R.id.gas_datepicker);
        int picker_day = simpleDatePicker.getDayOfMonth();
        int picker_month = simpleDatePicker.getMonth(); // get the selected month
        int picker_year = simpleDatePicker.getYear() - 1900; // get the selected year
        /*TODO: Combine these values, tack on time and what not, then convert back to date for use
        in grapy*/
//        TextView datetest = (TextView) v.findViewById(R.id.gas_date);
//        Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String s = formatter.format(datetest.getText().toString());

        SimpleDateFormat sdf = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy");
        String formatedDate = sdf.format(new Date(picker_year, picker_month, picker_day));

        // Parse back to date:
        try {
            Date date = sdf.parse(formatedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        TextView testline = (TextView) getView().findViewById(R.id.testline);
        testline.setText(formatedDate);


        // Get Date
        //String currentDate = DateFormat.getDateInstance(DateFormat.SHORT).format(new Date());


        Calendar calendar = Calendar.getInstance();
//        SimpleDateFormat df = new SimpleDateFormat("MMM-dd-yyyy");
//        String formattedDate = df.format(calendar.getTime());

        //KEEP
        String currentDate = calendar.getTime().toString();

    //    String currentDate = DateFormat.getDateInstance(DateFormat.SHORT).format(new Date());
        //testline.setText(Integer.toString(picker_day));

        // Get Time
        String currentTime = DateFormat.getTimeInstance(DateFormat.SHORT).format(new Date());

        // Set Notes
        //String notes="these are the notes";
        Spinner gas_car_selected = (Spinner) v.findViewById(R.id.spinner);
//        String text = gas_car_selected.getSelectedItem().toString();
//        text = text+"_GAS";

        /*if(mydb.insertContact(currentDate, currentTime, newMiles_str.getText().toString(),
                newPrice_str.getText().toString(), newGallons_str.getText().toString(),
                roundOff, notes.getText().toString(), text)){*/
        //KEEP*********************************
        if(mydb.insertContact(formatedDate, currentTime, newMiles_int,
                newPrice_int, newGallons_int, roundOff, notes.getText().toString(), text)){

            Toast.makeText(getActivity(), "Fillup mpg: " + roundOff , Toast.LENGTH_LONG).show();
        } else{
            Toast.makeText(getActivity(), "Something's not right", Toast.LENGTH_SHORT).show();
        }

        //Go back to summary page
        getActivity().setTitle("Summary");
        SummaryFragment_old fragment = new SummaryFragment_old();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_place, fragment, "fragment3");
        fragmentTransaction.commit();


        //Toast.makeText(getActivity(), "Fillup mpg:" + roundOff, Toast.LENGTH_SHORT).show();
        //break;

    }

/*    public int updateGasAvg(){
        int numRows = mydb.numberOfRows();
        int row =1;
        int avg_int = 0;
        while (row <= numRows) {
            Cursor rowdata = mydb.getData(row);

            rowdata.moveToFirst();

            avg_int = avg_int + rowdata.getInt(rowdata.getColumnIndex(DBHelper.CONTACTS_COLUMN_AVG));

            if (!rowdata.isClosed()) {
                rowdata.close();
            }
        }   row++;
        return avg_int;
    }*/
}



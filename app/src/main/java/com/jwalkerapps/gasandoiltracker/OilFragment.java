package com.jwalkerapps.gasandoiltracker;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class OilFragment extends Fragment implements View.OnClickListener {
private DBHelper mydb;
String table,oil_type;

    public OilFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_oil, container, false);
        mydb = new DBHelper(getActivity());
        getActivity().setTitle("Add Oil Change");
        Button btn_add_oil = (Button) v.findViewById(R.id.oil_add_button);
        btn_add_oil.setOnClickListener(this);
        final Spinner oil_dropdown_cars = (Spinner) v.findViewById(R.id.oil_spinner);

        oil_dropdown_cars.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                table = oil_dropdown_cars.getSelectedItem().toString();
                // Get ID
                String Carid = mydb.getID(position);
                table = table + Carid + "_OIL";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        List<String> oilTypeList = Arrays.asList("0w30", "0w40", "5w20", "5w30", "5w40","10w40","10w60","15w40");
        final Spinner oil_typeused = (Spinner) v.findViewById(R.id.oil_typeused);
        oil_typeused.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                oil_type = oil_typeused.getSelectedItem().toString();
                //carInfo_oil_type = carInfo_oil_type+"_GAS";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> oilType_adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, oilTypeList);
        oilType_adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        oil_typeused.setAdapter(oilType_adapter);
        oil_typeused.setSelection(0);

        Cursor oil_type = mydb.getspecData("OIL_TYPE","cars",table);
        //btn2.setOnClickListener(this);

        // Populate dropdown list with all cars
       // mydb = new DBHelper(getActivity());
        ArrayList<String> car_list = new ArrayList<>();
        car_list = mydb.getAllContacts("cars");
        ArrayAdapter<String> carList_adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, car_list);
        carList_adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        oil_dropdown_cars.setAdapter(carList_adapter);
        oil_dropdown_cars.setSelection(0);


        return v;
    }

    @Override
    public void onClick(View v) {
        mydb = new DBHelper(getActivity());

        // Get text from each field
        TextView milege =(TextView) getView().findViewById(R.id.oil_milege);
        //TextView oil_type =(TextView) getView().findViewById(R.id.oil_typeused);
        TextView oil_quantity =(TextView) getView().findViewById(R.id.oil_quantity);
        TextView notes =(TextView) getView().findViewById(R.id.oil_notes);
        double milege_int = Double.parseDouble( milege.getText().toString() );
        // Get Date
        String currentDate = DateFormat.getDateInstance(DateFormat.SHORT).format(new Date());

        // Get Time
        String currentTime = DateFormat.getTimeInstance(DateFormat.SHORT).format(new Date());


        if(mydb.addOilChange(currentDate, currentTime, milege_int,
                oil_type, oil_quantity.getText().toString(), notes.getText().toString(), table)) {

            Toast.makeText(getActivity(), "Oil Change Added", Toast.LENGTH_SHORT).show();
        }


        // Clear text in each field
        milege.setText("");
        //oil_type.setText("");
        oil_quantity.setText("");


        // Change Fragment back to summary page
        getActivity().setTitle("Summary");
        SummaryFragment_old fragment = new SummaryFragment_old();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_place, fragment, "fragment3");
        fragmentTransaction.commit();
    }
}
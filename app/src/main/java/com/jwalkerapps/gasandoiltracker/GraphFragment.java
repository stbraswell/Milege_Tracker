package com.jwalkerapps.gasandoiltracker;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class GraphFragment extends Fragment {
    private DBHelper mydb;
    Date date;
    Date maxdate;
    Date mindate;

    public GraphFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_graph, container, false);
        mydb = new DBHelper(getActivity());

        // Get Bundled data
        Bundle b = getArguments();
        String s = b.getString("car");
        int position = b.getInt("position");
        String car2Graph = s + position +"_GAS";
        getActivity().setTitle("Summary: " + s);

        int numOfRows = mydb.numberOfRows(car2Graph);
        if (numOfRows > 0) {

            // TESTING: Sending date to text view for testing
            ArrayList<String> dateDB = mydb.getDataFromColumn(car2Graph, "date");
            SimpleDateFormat sdf2 = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy",
            //SimpleDateFormat sdf2 = new SimpleDateFormat("MM dd yyyy",
                    Locale.ENGLISH);
            try {
                date = sdf2.parse(dateDB.get(0));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            TextView testtext = (TextView) v.findViewById(R.id.grapth_test_text);
            testtext.setText(car2Graph);

            //Cursor oil_type = mydb.getspecData("OIL_TYPE",car2Graph+"_GAS",car2Graph); //column,table,car

            TextView testtext2 = (TextView) v.findViewById(R.id.grapth_test_text2);
            //testtext2.setText(date.toString());

            DataPoint[] datapoints = new DataPoint[numOfRows];

            ArrayList<String> avgDB = mydb.getDataFromColumn(car2Graph, "avg");
            for (int i = 0; i < numOfRows; i++) {
                SimpleDateFormat sdf = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy",
                //SimpleDateFormat sdf = new SimpleDateFormat("M d yy",
                        Locale.ENGLISH);
                try {
                    date = sdf.parse(dateDB.get(i));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                datapoints[i] = new DataPoint(date, Double.parseDouble(avgDB.get(i)));
                if (i == 0) {
                    mindate = date;
                }
                if (i == (numOfRows -1)) {
                    maxdate = date;
                }
            }


            testtext2.setText(maxdate.toString());

            GraphView graph = (GraphView) v.findViewById(R.id.graph);
            LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(datapoints);
            //graph.getViewport().setMaxX(3.0);

            graph.getViewport().setScrollable(true); // enables horizontal scrolling
            graph.getViewport().setScrollableY(true); // enables vertical scrolling
            graph.getViewport().setScalable(true); // enables horizontal zooming and scrolling
            graph.getViewport().setScalableY(true); // enables vertical zooming and scrolling
            graph.addSeries(series);
            // set date label formatter
            graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getActivity()));
            graph.getGridLabelRenderer().setNumHorizontalLabels(numOfRows); // only 4 because of the space

            // set manual x bounds to have nice steps
            graph.getViewport().setMinX(mindate.getTime());
            graph.getViewport().setMaxX(maxdate.getTime());
            graph.getViewport().setXAxisBoundsManual(true);

            // as we use dates as labels, the human rounding to nice readable numbers
            // is not necessary
            graph.getGridLabelRenderer().setHumanRounding(false);
        } else {
            Toast.makeText(getActivity(), "No Fillups to display", Toast.LENGTH_SHORT).show();
        }
        return v;
    }

}

package com.jwalkerapps.gasandoiltracker;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SummaryFragment_old extends Fragment implements RVAdapter.OnItemClicked, RVAdapter.OnItemLongClicked {
    DBHelper mydb_summary;
    String car_table = "cars";
    ArrayList carListGas;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Person> persons;
    private RecyclerView rv;
    private Context context;
    private Activity mActivity;
    String nextOilChange;
    private static final String TAG = "MyActivity";

    public SummaryFragment_old() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Dashboard");
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_summary, container, false);
        //context = getActivity();
        rv=(RecyclerView) v.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);

        initializeData();
        initializeAdapter();
        return v;
    }

    private void initializeData(){
        persons = new ArrayList<>();
        if (isTableExists(car_table, true)) {
            carListGas = mydb_summary.getAllContacts(car_table);
            TextView sumtest=(TextView) getActivity().findViewById(R.id.info_text);
            Log.d("HHHHHHH", String.valueOf(carListGas));
            for (int i = 0; i < carListGas.size(); i++) {

                // Get ID
                String id = mydb_summary.getID(i);

                //Get Year
                String car_year = mydb_summary.getspecData2("YEAR", "cars", id, "id");
                // Get Car Name
                String news = carListGas.get(i).toString();
                // Concat
                String name_view = car_year + " " + news;


                // Get MPG
                int avg_mgp = mydb_summary.updateGasAvg(news + id + "_GAS");
                // Get Oil info
                int lastOilRow = mydb_summary.numberOfRows(news + id + "_OIL");
                //String oilInt_str = mydb_summary.getspecData2("OIL_INTERVAL", "cars", news, "MODEL");
                String oilInt_str = mydb_summary.getspecData2("OIL_INTERVAL", "cars", id, "id");
                if (oilInt_str != null) {
                    Log.d("SUMFRAG", oilInt_str);
                } else {
                    Log.d("SUMFRAG", "oilInt_str: was null");
                }
                int oilInt_int = Integer.parseInt(oilInt_str);
                String lastOilMiles = mydb_summary.getspecData2("miles", news + id + "_OIL", Integer.toString(lastOilRow), "id");
                if (lastOilMiles != null) {
                    Double lastOilMiles_int = Double.parseDouble(lastOilMiles);
                    Double nextOilMiles = oilInt_int + lastOilMiles_int;
                    nextOilChange = Double.toString(nextOilMiles);
                } else {
                    nextOilChange = "No Oil Data Yet";
                }

                //String carPic = mydb_summary.getspecData2("PICTURE", "cars", news, "MODEL");
                String carPic = mydb_summary.getspecData2("PICTURE", "cars", id, "id");
                String filename = "car_" + id + ".jpg";
                File pathName = new File(getContext().getFilesDir(), filename);
                //String pathName = "//data//"+ "//data//" + "com.jwalkerapps.gasandoiltracker"
                  //      + "//databases//" + filename;
                Log.d("sumfrag",carPic);
                Log.d(TAG, String.valueOf(pathName));
                //sumtest.setText(pathName);
                //if(pathName.exists()){

                    //Bitmap myBitmap = BitmapFactory.decodeFile(pathName.getAbsolutePath());
                int resID = getResources().getIdentifier(String.valueOf(pathName),
                            "drawable", getContext().getPackageName());
                Log.d(TAG, String.valueOf(resID));
                    //ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);

                    //myImage.setImageBitmap(myBitmap);

                if(carPic != null && !carPic.isEmpty()) {
                //if (fileExist(String.valueOf(pathName))) {
                    //String pathName = "/path/to/file/xxx.jpg";
                    //Drawable carPic = Drawable.createFromPath(String.valueOf(pathName));
                    //Bitmap carPic = BitmapFactory.decodeFile(String.valueOf(pathName));

                    persons.add(new Person(car_year, news, "Average Gas Mileage: "+Integer.toString(avg_mgp) + " mpg", "Next Oil Change: " + nextOilChange, carPic,id));
                } else {
                    //Bitmap icon = BitmapFactory.decodeResource(getContext().getResources(),
                           // R.drawable.car1a);
                    Uri icon = Uri.parse("android.resource://com.jwalkerapps.gasandoiltracker/drawable/car1a.png");
                    persons.add(new Person(car_year, news, "Average Gas Mileage: "+Integer.toString(avg_mgp) + " mpg", "Next Oil Change: " + nextOilChange, String.valueOf(icon),id));
                }
            }
        }
    }

    private void initializeAdapter(){
        RVAdapter adapter = new RVAdapter(persons,getActivity());
        rv.setAdapter(adapter);
        adapter.setOnClick(this); // Bind the listener
        adapter.setOnLongClick(this);
    }

    public boolean isTableExists(String tableName, boolean openDb) {
        mydb_summary = new DBHelper(getActivity());
        SQLiteDatabase mDatabase = null;
        //String mDatabase ="MyGasDB.db"
        if (openDb) {
            if (mDatabase == null || !mDatabase.isOpen()) {
                mDatabase = mydb_summary.getReadableDatabase();
            }

            if (!mDatabase.isReadOnly()) {
                mDatabase.close();
                mDatabase = mydb_summary.getReadableDatabase();
            }
        }

        Cursor cursor = mDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }
    public boolean fileExist(String fname){
        File file = getContext().getFileStreamPath(fname);
        return file.exists();
    }


    @Override
    public void onItemClick(int position) {
        ArrayList<String> carDB = mydb_summary.getDataFromColumn("cars", "MODEL");
        String carGraph = persons.get(position).name;
        Log.d("CAR NAME#####",carGraph);
        // Get ID
        String id = mydb_summary.getID(position);
        Bundle i = new Bundle();
        i.putString("car", carGraph);
        i.putInt("position", Integer.parseInt(id));
        //getActivity().setTitle("Summary: " + carGraph);
        GraphFragment frag = new GraphFragment();
        frag.setArguments(i);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_place
                        , frag)
                .addToBackStack(null)
                .commit();
    }

    public void onItemLongClick(final int position){
        //Toast.makeText(getActivity(), "Long Click" , Toast.LENGTH_LONG).show();
        final CharSequence[] items = {"Edit", "Delete"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Select The Action");


        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
               // Toast.makeText(getActivity(), Integer.toString(item) , Toast.LENGTH_LONG).show();
                // Get ID
                String id = mydb_summary.getID(position);
                switch (item) {
                    case 0: //EDIT
                        Log.d("OPTIONS", String.valueOf(item));

                        //Toast.makeText(getActivity(), id , Toast.LENGTH_LONG).show();
                        Bundle i = new Bundle();
                        //i.putString("car", carGraph);
                        i.putString("position", id);
                        Log.d("OPTIONS", id);
                        //getActivity().setTitle("Summary: " + carGraph);
                        EditCarFragment frag = new EditCarFragment();
                        frag.setArguments(i);
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.fragment_place
                                        , frag)
                                .addToBackStack(null)
                                .commit();
                        break;
                    case 1: //DELETE
                        Log.d("OPTIONS", String.valueOf(item));
                        //Toast.makeText(getActivity(), item , Toast.LENGTH_LONG).show();
                        mydb_summary.deleteCar(persons.get(position).name, id);

                        String dir = getContext().getFilesDir().getAbsolutePath();
                        String filename = "car_" + id + ".jpg";
                        File fileToDelete = new File(getContext().getFilesDir(),filename);
                        // File f0 = new File(dir, "myFile");
                        boolean d0 = fileToDelete.delete();
                        Log.w("Delete Check", "File deleted: " + dir + "/" + filename + " " + d0);
                        refreshFragment();
                        //Toast.makeText(getActivity(), persons.get(position).name , Toast.LENGTH_LONG).show();
                }
            }
        });
        builder.show();
    }

    public void refreshFragment() {
        getFragmentManager().beginTransaction().detach(this).attach(this).commit();
    }

/*        getView();
        Button tstbtn = (Button) v.findViewById(R.id.summarybtn);
        tstbtn.setOnClickListener(this);
        mydb_summary = new DBHelper(getActivity());

//TODO loop over cars and add each group of views....

        // Get list of cars
        if (isTableExists(car_table, true)) {
            carListGas = mydb_summary.getAllContacts(car_table);

            for (int i = 0; i < carListGas.size(); i++) {

                String news = carListGas.get(i).toString();
                LinearLayout main = (LinearLayout) v.findViewById(R.id.summaryLinear);
                LinearLayout linearLayoutCar = new LinearLayout(getContext());
                linearLayoutCar.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                        LayoutParams.WRAP_CONTENT));
                linearLayoutCar.setOrientation(LinearLayout.VERTICAL);
                linearLayoutCar.setPadding(0, 0, 0, 25);
                String carBlockID = "car_" + i + "_block";
                linearLayoutCar.setClickable(true);
                int blockID = getResources().getIdentifier(carBlockID, "id", getContext().getPackageName());
                linearLayoutCar.setId(blockID);
                linearLayoutCar.setOnClickListener(this);
                main.addView(linearLayoutCar);

                // Add TextView for new car to summary page
                LayoutParams lparams = new LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                lparams.gravity = Gravity.CENTER;
                TextView tv = new TextView(getContext());
                tv.setLayoutParams(lparams);
                String carID = "car_" + i;
                int carID_int = getResources().getIdentifier(carID, "id", getContext().getPackageName());
                tv.setId(carID_int);
                tv.setText(news);
                tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
                linearLayoutCar.addView(tv);

                //TODO: Change this to relative layout
                // Create new LinearLayout
                LinearLayout linearLayout = new LinearLayout(getContext());
                linearLayout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT));
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout.setPadding(0, 0, 0, 0);

                // Add textviews
                TextView textView1 = new TextView(getContext());
                textView1.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT));
                textView1.setText("Average Gas Milege:");
                linearLayout.addView(textView1);

                TextView textView2 = new TextView(getContext());
                LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT);
                textView2.setLayoutParams(layoutParams);
                String fieldID = "car_" + i + "_mpg";
                int resID = getResources().getIdentifier(fieldID, "id", getContext().getPackageName());
                textView2.setId(resID);
                // Get MPG
                int avg_mgp = mydb_summary.updateGasAvg(news + "_GAS");
                textView2.setText(Integer.toString(avg_mgp) + " mpg");
                textView2.setPadding(10, 0, 5, 0);
                linearLayout.addView(textView2);

                // Next Oil Change
                TextView nextOilChange = new TextView(getContext());
                LayoutParams layoutParams_oil = new LayoutParams(LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT);
                nextOilChange.setLayoutParams(layoutParams_oil);
                String oilID_str = "car_" + i + "_mpg";
                int oilID_int = getResources().getIdentifier(oilID_str, "id", getContext().getPackageName());
                nextOilChange.setId(oilID_int);
                // Get Oil info
                int lastOilRow = mydb_summary.numberOfRows(news+"_OIL");
                String oilInt_str = mydb_summary.getspecData2("OIL_INTERVAL", "cars", news, "MODEL");
                int oilInt_int = Integer.parseInt(oilInt_str);
                String lastOilMiles = mydb_summary.getspecData2("miles", news+"_OIL", Integer.toString(lastOilRow), "id");

                if (lastOilMiles != null){
                    Double lastOilMiles_int = Double.parseDouble(lastOilMiles);
                Double nextOilMiles = oilInt_int + lastOilMiles_int;
                //int nextOilMilege = mydb_summary.getspecData2());
                nextOilChange.setText("Next oil change: "+Double.toString(nextOilMiles) + " miles");
                } else{
                    nextOilChange.setText("No data yet");
                }

                nextOilChange.setPadding(0, 0, 5, 0);

                //linearLayout.addView(nextOilChange);

                // Add the linear view created above to the existing (main)linear layout
                linearLayoutCar.addView(linearLayout);
                linearLayoutCar.addView(nextOilChange);
                // Create the red line between cars
                View viewLine = new View(getContext());
                LayoutParams layoutParams3 = new LayoutParams(LayoutParams.MATCH_PARENT,
                        LayoutParams.WRAP_CONTENT);
                viewLine.setLayoutParams(layoutParams3);
                // Convert 1 dp to px (height only takes px)
                int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, getResources().getDisplayMetrics());
                layoutParams3.height = height;
                // Convert 10 dp to px (bottomMargin only takes px)
                int botMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics());
                layoutParams3.bottomMargin = botMargin;
                viewLine.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                viewLine.setLayoutParams(layoutParams3);

                // Add red line to the existing (main) linear layout
                linearLayoutCar.addView(viewLine);
            }
        }
        return v;
    }
    @Override
    public void onResume() {
        super.onResume();
        // Set title
        getActivity().setTitle("Dashboard");
    }





    public boolean isTableExists(String tableName, boolean openDb) {
        mydb_summary = new DBHelper(getActivity());
        SQLiteDatabase mDatabase = null;
        //String mDatabase ="MyGasDB.db"
        if (openDb) {
            if (mDatabase == null || !mDatabase.isOpen()) {
                mDatabase = mydb_summary.getReadableDatabase();
            }

            if (!mDatabase.isReadOnly()) {
                mDatabase.close();
                mDatabase = mydb_summary.getReadableDatabase();
            }
        }

        Cursor cursor = mDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }*/



 /*   @Override
    public void onClick(View v) {

        switch (v.getId() *//* to get clicked view id*//*) {

            case R.id.car_0_block:
                goToGraph(0);
                break;
            case R.id.car_1_block:
                goToGraph(1);
                break;
            case R.id.car_2_block:
                goToGraph(2);
                break;
            case R.id.car_3_block:
                goToGraph(3);
                break;
            case R.id.summarybtn:
                //String oilInt = mydb_summary.getspecData2("OIL_INTERVAL", "cars", "WRX", "MODEL");
                String oilInt = Environment.getExternalStorageDirectory().toString();
                //getExternalStorageDirectory : /storage/emulated/0
                //getDataDirectory: /data
                TextView sumtest=(TextView) getActivity().findViewById(R.id.summary_test);
                sumtest.setText(oilInt);

        }
    }

    public void goToGraph(int id) {
        ArrayList<String> carDB = mydb_summary.getDataFromColumn("cars", "MODEL");
        String carGraph = carDB.get(id);
        Bundle i = new Bundle();
        i.putString("car", carGraph);
        getActivity().setTitle("Summary: " + carGraph);
        GraphFragment frag = new GraphFragment();
        frag.setArguments(i);
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_place
                        , frag)
                .addToBackStack(null)
                .commit();
    }*/


}
package com.jwalkerapps.gasandoiltracker;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.PersonViewHolder> {
    private static final String TAG = "MyActivity";

    //declare interfaces
    private OnItemClicked onClick;
    private OnItemLongClicked onLongClick;

    //make interfaces like this
    public interface OnItemClicked {
        void onItemClick(int position);
    }
    public interface OnItemLongClicked {
        void onItemLongClick(int position);
    }

    public class PersonViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView caryear;
        TextView personName;
        TextView personAge;
        TextView carOil;
        ImageView personPhoto;


        PersonViewHolder(View itemView, final int i) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            caryear = (TextView)itemView.findViewById(R.id.car_year);
            personName = (TextView)itemView.findViewById(R.id.person_name);
            personAge = (TextView)itemView.findViewById(R.id.person_age);
            carOil = (TextView)itemView.findViewById(R.id.person_oil);
            personPhoto = (ImageView)itemView.findViewById(R.id.person_photo);
            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "--------------Click----------------");
                    Log.d("hello", String.valueOf(i));
                    onClick.onItemClick(i);
                }});
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    onLongClick.onItemLongClick(i);
                    return true;
                }
            });*/
        }
    }

    List<Person> persons;
    Context context;
    RVAdapter(List<Person> persons,Context context){
        this.persons = persons;
        this.context=context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v,i);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, final int i) {
        //sumFrag = new SummaryFragment_old();

        personViewHolder.caryear.setText(persons.get(i).year);
        personViewHolder.personName.setText(persons.get(i).name);
        personViewHolder.personAge.setText(persons.get(i).age);
        personViewHolder.carOil.setText(persons.get(i).oil);
       //selectedImagePreview.setImageBitmap(new UserPicture(selectedImageUri, getContext().getContentResolver()).getBitmap());
       // personViewHolder.personPhoto.setImageBitmap(persons.get(i).photoId);
        Uri userPic = Uri.parse(String.valueOf(persons.get(i).photoId));
        try {
            personViewHolder.personPhoto.setImageBitmap(new UserPicture(userPic, context.getContentResolver()).getBitmap());
        } catch (IOException e) {
            e.printStackTrace();
        }
        personViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "--------------Click----------------");
                Log.d("hello", String.valueOf(i));
                onClick.onItemClick(i);
            }});
        personViewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    onLongClick.onItemLongClick(i);
                    return true;
            }
        });

    }

    public void setOnClick(OnItemClicked onClick)
    {
        this.onClick=onClick;
    }
    public void setOnLongClick(OnItemLongClicked onLongClick)
    {
        this.onLongClick=onLongClick;
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }
}

package com.jwalkerapps.gasandoiltracker;

/**
 * Created by Troy on 2/4/17.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

/*
*TODO: DEAL WITH carModel / AddCarFragment.getVariable() -
* - if it returns a value then a car is being added {THIS WONT HAPPEN TOO OFTEN}
* - if it returns null then it is either the first time the app is started {once} or
*   the car tables already exist {MOST OF THE TIME}
 */

/*

 */

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyGasDB.db";


    // Table for Gas Info
    //public static final String CONTACTS_TABLE_NAME = "gasgas";
    //public static final String CONTACTS_TABLE_NAME = carModel;
    public static final String CONTACTS_COLUMN_ID = "id";
    public static final String CONTACTS_COLUMN_NAME = "date";
    public static final String CONTACTS_COLUMN_TIME = "time";
    public static final String CONTACTS_COLUMN_EMAIL = "miles";
    public static final String CONTACTS_COLUMN_STREET = "price";
    public static final String CONTACTS_COLUMN_CITY = "gallons";
    public static final String CONTACTS_COLUMN_AVG = "avg";
    public static final String CONTACTS_COLUMN_PHONE = "notes";

    // Table of Car Info
    public static final String CARINFO_TABLE_NAME = "cars";
    public static final String CARINFO_COLUMN_ID = "id";
    public static final String CARINFO_COLUMN_YEAR = "YEAR";
    public static final String CARINFO_COLUMN_MAKE = "MAKE";
    public static final String CARINFO_COLUMN_MODEL = "MODEL";
    public static final String CARINFO_COLUMN_OIL_TYPE = "OIL_TYPE";
    public static final String CARINFO_COLUMN_OIL_QUANT = "OIL_QUANTITY";
    public static final String CARINFO_COLUMN_OIL_INTERVAL = "OIL_INTERVAL";
    public static final String CARINFO_COLUMN_URI = "PICTURE";
    private HashMap hp;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        Log.d("DB CREATION","Creating the DB");
        db.execSQL(
                "create table " + CARINFO_TABLE_NAME +
                        "(id integer primary key, YEAR text,MAKE text, MODEL text, " + CARINFO_COLUMN_OIL_TYPE + " text, " + CARINFO_COLUMN_OIL_QUANT + " text, "+ CARINFO_COLUMN_OIL_INTERVAL+ " text, " + CARINFO_COLUMN_URI+" text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS contacts");
        onCreate(db);
    }


    // Create Gas table for each car
    public boolean createNewCar(int id, String year, String make, String model, String oil_type, String oil_quant, Context context) {
        SQLiteDatabase db = this.getWritableDatabase();
        //boolean exists = CheckIsDataAlreadyInDBorNot("cars", "MODEL", model);
        //ArrayList carListGas = getAllContacts(CARINFO_TABLE_NAME);
        //int dbSize = carListGas.size();
        //if (!exists) {
        String model_gas = model + id + "_GAS";
        Log.d("DBHELPER",model_gas);
        String model_oil = model + id + "_OIL";
        Log.d("DBHELPER",model_oil);
        db.execSQL(
                "create table " + model_gas +
                        "(id integer primary key, date text,time text, miles text, price text, gallons text, avg text,notes text)"
        );
        db.execSQL(
                "create table " + model_oil +
                        "(id integer primary key, date text,time text, miles text, oil_used text, quantity text, notes text)"
        );
        /*} else {
            Toast.makeText(context, model + " already exists", Toast.LENGTH_SHORT).show();

        }*/
        return true;
    }

    // Create Gas table for each car
    public boolean insertNewCar(int id, String year, String make, String model, String oil_type, String oil_quant, String oil_interval, String picture_uri, Context context) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        boolean exists = CheckIsDataAlreadyInDBorNot("cars", "MODEL", model);
        Log.d("DBHELPER","exists: " + exists);
        //if (!exists) {
        contentValues.put("id", id);
        contentValues.put("YEAR", year);
        contentValues.put("MAKE", make);
        contentValues.put("MODEL", model);
        contentValues.put("OIL_TYPE", oil_type);
        contentValues.put("OIL_QUANTITY", oil_quant);
        contentValues.put("OIL_INTERVAL", oil_interval);
        contentValues.put("PICTURE", picture_uri);
        db.insert(CARINFO_TABLE_NAME, null, contentValues);
        //}
        return true;
    }

    // Get ID from row (iterating)
    public String getID(int position) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c=db.rawQuery("SELECT * FROM cars", null);
        c.moveToPosition(position);
        Log.d("POSITION", String.valueOf(position));
        String carID = c.getString(c.getColumnIndex(CARINFO_COLUMN_ID));
        return carID;
    }

    // Create Gas table for each car
    public boolean updateCarInfo(String year, String make, String model, String oil_type, String oil_quant, String oil_interval, String picture_uri, String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        //boolean exists = CheckIsDataAlreadyInDBorNot("cars", "MODEL", model);
        Log.d("DBHELPER", String.valueOf(contentValues));
        //if (!exists) {
        contentValues.put("YEAR", year);
        contentValues.put("MAKE", make);
        contentValues.put("MODEL", model);
        contentValues.put("OIL_TYPE", oil_type);
        contentValues.put("OIL_QUANTITY", oil_quant);
        contentValues.put("OIL_INTERVAL", oil_interval);
        contentValues.put("PICTURE", picture_uri);
        //db.insert(CARINFO_TABLE_NAME, null, contentValues);
        db.update(CARINFO_TABLE_NAME,contentValues,"id = ? ",new String[]{id});
        //}
        return true;
    }

   /* public boolean updateContact(Integer id, String date, String miles, String price, String gallons, String notes) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);
        contentValues.put("miles", miles);
        contentValues.put("price", price);
        contentValues.put("gallons", gallons);
        contentValues.put("notes", notes);
        db.update("gas", contentValues, "id = ? ", new String[]{Integer.toString(id)});
        return true;
    }*/

    // Check for duplicate car
    public boolean checkDBForDuplicate(String carModel) {

        return true;
    }

    // Delete Car from DB
    public boolean deleteCar(String name,String id){
        //String[] carInfoArray = name.split(" ");
        //String carName = carInfoArray[1];
        SQLiteDatabase db = this.getReadableDatabase();
        // Delete Car from 'cars' table
        db.delete(CARINFO_TABLE_NAME,
                "id = ? ",
                new String[]{id});

        // Table names
        String gasTable = name + id + "_GAS";
        String oilTable = name + id + "_OIL";
        String[] tables = {gasTable,oilTable};

        // Drop the gas and oil tables
        for (int i = 0; i < tables.length; i++) {
            db.execSQL("DROP TABLE IF EXISTS " + tables[i]);
        }
        return true;
    };

    // Add a Fillup to Gas table
    public boolean insertContact(String date, String time, double miles, double price, double gallons, double avg, String notes, String table) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);
        contentValues.put("time", time);
        contentValues.put("miles", miles);
        contentValues.put("price", price);
        contentValues.put("gallons", gallons);
        contentValues.put("avg", avg);
        contentValues.put("notes", notes);
        db.insert(table, null, contentValues);
        return true;
    }

    // Add an oil change
    public boolean addOilChange(String date, String time, double miles, String oil, String quantity, String notes, String table) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);
        contentValues.put("time", time);
        contentValues.put("miles", miles);
        contentValues.put("oil_used", oil);
        contentValues.put("quantity", quantity);
        contentValues.put("notes", notes);
        db.insert(table, null, contentValues);
        return true;
    }

    public Cursor getData(int id, String table) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + table + " where id=" + id + "", null);
        return res;
    }
    public Cursor getspecData(String column, String table, String car) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select " + column + " from " + table + " where MODEL=" + car + "", null);
        return res;

        /*SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select "+id+" from " + table, null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            String carName = res.getString(res.getColumnIndex(column));
            res.moveToNext();
        }
        if (res != null && !res.isClosed()){
            res.close();
            db.close();
        }
        return carName;*/
    }

    public String getspecData2(String columnWant, String table, String car, String columnKnown) {   //column want, table, column lookup, id to find)
        String place = null;
        // ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        /*Cursor res = db.rawQuery("select * from " + table + " where MODEL='" + car +"'", null);
        res.moveToFirst();
        String test = res.getString(0);*/

//String[] columns = new String[]{column};
        String[] params = new String[]{car};
        Cursor c = db.query(table, null,
                columnKnown + " = ?", params,
                //            "MODEL = ?", params,
                null, null, null);
        if (c != null) {
            if (c.moveToFirst()) {
                int placeColumn = c.getColumnIndex(columnWant);
                //            int placeColumn = c.getColumnIndex("MODEL");
                place = c.getString(placeColumn);
            }
            c.close();
            return place;

        }
        return "";
    }


    // RETURNS NUMBER OF ROWS FROM TABLE
    // TODO: ADD table parameter: numberOfRows(String table)
    //TODO: Update all calls to this function to use table arg
    public int numberOfRows(String table) {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, table);
        return numRows;
    }


    //HOLDOVER FROM EXAMPLE
    public boolean updateContact(Integer id, String date, String miles, String price, String gallons, String notes) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);
        contentValues.put("miles", miles);
        contentValues.put("price", price);
        contentValues.put("gallons", gallons);
        contentValues.put("notes", notes);
        db.update("gas", contentValues, "id = ? ", new String[]{Integer.toString(id)});
        return true;
    }


    //HOLDOVER FROM EXAMPLE
    public Integer deleteContact(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("gas",
                "id = ? ",
                new String[]{Integer.toString(id)});
    }


    //TODO: Update all calls with table parameter
    public ArrayList<String> getAllContacts(String table) {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + table, null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(res.getString(res.getColumnIndex(CARINFO_COLUMN_MODEL)));
            res.moveToNext();
        }
        return array_list;
    }


    public ArrayList<String> getIndivCarInfo(String id) {
        ArrayList<String> array_list = new ArrayList<String>();
        String[] carColumns = {CARINFO_COLUMN_YEAR, CARINFO_COLUMN_MAKE, CARINFO_COLUMN_MODEL, CARINFO_COLUMN_OIL_TYPE, CARINFO_COLUMN_OIL_QUANT, CARINFO_COLUMN_OIL_INTERVAL, CARINFO_COLUMN_URI};
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.query(CARINFO_TABLE_NAME,null,"id = ? ",new String[]{id},null,null,null);
        //Cursor res = db.rawQuery("select * from " + CARINFO_TABLE_NAME + "where id='" + Integer.toString(id) +"'", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            for (int i = 0; i < carColumns.length; i++) {
                array_list.add(res.getString(res.getColumnIndex(carColumns[i])));
            }
            res.moveToNext();

        }
            return array_list;
        }


    public ArrayList<String> getDataFromColumn(String table, String column) {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        //SQLiteDatabase db = this.getReadableDatabase();
        SQLiteDatabase db = getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + table, null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(res.getString(res.getColumnIndex(column)));
            res.moveToNext();
        }
            if (res != null && !res.isClosed()){
                res.close();
            db.close();
        }
        return array_list;
    }


    // RETURN GAS AVERAGE MGP
    public int updateGasAvg(String table) {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = this.numberOfRows(table);
        int avg_int;
        if (numRows > 0) {
            int row = 1;
            avg_int = 0;
            while (row <= numRows) {
                Cursor rowdata = this.getData(row, table);

                rowdata.moveToFirst();

                avg_int = avg_int + rowdata.getInt(rowdata.getColumnIndex(DBHelper.CONTACTS_COLUMN_AVG));

                if (!rowdata.isClosed()) {
                    rowdata.close();
                }
                row++;
            }
            avg_int = avg_int / numRows;
        } else {
            avg_int = 0;
        }
        return avg_int;
    }


    //NOT SURE THIS IS NEEDED ANYMORE
    public String[] carList() {
        //SQLiteDatabase db = this.getReadableDatabase();
        int numRows = this.numberOfRows("cars");
        int row = 1;
        String[] listOfCars = new String[numRows];
        while (row <= numRows) {
            Cursor rowdata = this.getData(row, CARINFO_TABLE_NAME);

            rowdata.moveToFirst();
            String carname = rowdata.getString(rowdata.getColumnIndex(DBHelper.CARINFO_COLUMN_MODEL));
            listOfCars[row] = carname;
        }
        return listOfCars;
    }


    public boolean CheckIsDataAlreadyInDBorNot(String TableName,
                                               String dbfield, String fieldValue) {
        SQLiteDatabase db = this.getWritableDatabase();
        String Query = "Select * from " + TableName + " where " + dbfield + " = \"" + fieldValue + "\";";
        Cursor cursor = db.rawQuery(Query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

/*    public int getOilInterval(String car){




        return oilInt;
    }*/
}

